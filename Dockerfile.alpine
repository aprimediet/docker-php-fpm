ARG REGISTRY_URL=git.winteraccess.id/docker
ARG PHP_VERSION=8.3

FROM ${REGISTRY_URL}/php:${PHP_VERSION}-alpine
LABEL maintainer="<Muhamad Aditya Prima> aprimediet@gmail.com"

ENV PHP_FPM_BIN=php-fpm${PHP_VERSION}

# Set workdir
WORKDIR /

# INSTALL PHP APP
RUN --mount=type=cache,target=/var/cache/apk \
    apk upgrade && apk add --update \
    php${PHP_VERSION}-fpm

# REMOVE CURRENT POOL FIRST
RUN rm -rf /etc/php${PHP_VERSION}/php-fpm*

# COPY CONFIGURATION FILES
RUN mkdir -p /etc/php/fpm
ADD ./etc /etc

# COPY INIT SCRIPT
ADD ./scripts/php-fpm-init-alpine /usr/local/bin/php-fpm-init
RUN chmod 755 /usr/local/bin/php-fpm-init

# INSTALL PHP FPM HEALTHCHECK UTILITIES
ADD https://raw.githubusercontent.com/renatomefi/php-fpm-healthcheck/master/php-fpm-healthcheck /usr/local/bin
RUN chmod 755 /usr/local/bin/php-fpm-healthcheck

# CLEAN APK CACHES
RUN rm -vrf /var/cache/apk/*

EXPOSE 9000

CMD ["sh","-c", "/usr/local/bin/php-fpm-init"]